import { createRouter, createWebHistory } from "vue-router";
import Counter from '@/views/Counter.vue';
import InputModel from '@/views/InputModel.vue';
import StudentForm from '@/views/StudentForm.vue';
import Loop from '@/views/Loop.vue';
import NotFound from '@/views/NotFound.vue';

export const router  = createRouter({
    history: createWebHistory(),
    // linkActiveClass: 'active',
    routes: [
        {path: '/', component: Counter},
        {path: '/loop', component: Loop},
        {path: '/student-form', component: StudentForm},
        {path: '/input', component: InputModel},
        {path: '/:pathMatch(.*)*', component: NotFound}
    ]
});