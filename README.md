# first-vue

Premier projet Vue.js dans lequel on voit les syntaxes et structures de base du Framework sans interaction avec un backend.

## How To Use
1. Cloner le projet
2. Faire un `npm i` (si des erreurs restent dans vscode après ça recharger la fenêtre : ctrl+shift+p -> reload window)
3. Lancer le serveur de dev `npm run dev` et voir le site sur http://localhost:5173

## Exercices
### Exo Compteur
1. Dans le script du App.vue, créer une nouvelle ref count avec 0 dedans par défaut
2. Créer une fonction increment qui augmente la valeur de count et une decrement qui la fait baisser
3. Dans le template, afficher la valeur de count et mettre 2 button qui au click exécuteront les fonction increment et decrement
4. Créer un dossier components dans le src et dedans créer un fichier Counter.vue qui contiendra un template et un script comme dans le App
5. Couper-coller les ref et fonctions propre au compteur du App vers le Counter et pareil pour le html
6. Dans le template du App, rajouter une balise <Counter /> qui normalement fera un import du component

### Boucle
1. Créer un nouveau component Loop.vue dans le dossier components
2. Dans son script créer une ref qui va contenir un tableau de prénom et on va l'appeler students
3. Dans le template, créer un ul et dedans mettre un li et dans les chevrons de ce li mettre un v-for="item of students" et faire en sorte d'afficher la variable item dans le li
4. Charger le component Loop dans le App.vue

### Lier une variable à un input
1. Créer un component InputModel.vue avec un template et un script setup, charger ce component dans le App en dessous des deux autres
2. Dans ce component, créer une ref avec une string dedans et afficher la dans le template
3. Rajouter un input et sur cet input, mettre un v-model dans lequel on place la variable qui contient notre ref
4. Constater ce qu'il se passe quand on tape dans l'input
5. rajouter un paragraphe avec "message is too long" dedans qui aura un v-if dessus pour qu'il s'affiche seulement si la ref liée à l'input fait une length supérieur à 10
Bonus:  Rajouter un span à côté de l'input avec la length de la ref qui s'affiche en direct, et faire qu'elle devienne rouge si jamais elle dépasse 10

### Choix de l'affichage (via un select)
1. Dans le App.vue créer une nouvelle ref qu'on va appeler display avec comme valeur par défaut 'counter'
2. Dans le template, créer un select relié à la variable display via un v-model et dans ce select mettre 3 <option> counter, loop et input 
3. Dans le template du App toujours, rajouter un v-if sur le <Counter />, le <Loop /> et le <InputModel /> pour faire en sorte d'afficher l'un ou l'autre selon la valeur de display

### StudentForm
1. Créer un nouveau component StudentForm.vue (si vous avez le select et le display, rajouter une option dans le App pour pouvoir afficher le StudentForm, sinon juste mettez le comme ça dans le template)
2. Dans ce component, mettre un form dans le template qui aura 2 champs, un nom en text et un age en number
3. Dans le src, créer un fichier entities.ts avec une interface Student dedans qui aura donc un name:string et un age:number
4. Dans le script du StudentForm, créer une ref<Student> avec un objet dedans avec des valeurs par défaut pour les 2 propriétés
5. Lier ces student.name à un input via un v-model et student.age à l'autre input
6. Créer une fonction handleSubmit() qui va faire un console.log de la value de la ref pour le moment et l'assigner au @submit.prevent du form (.prevent pour faire le preventDefault automatiquement)